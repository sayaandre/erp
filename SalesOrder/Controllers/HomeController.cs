﻿using EnDecDll;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SalesOrder.Models.Home;

namespace SalesOrder.Controllers
{
    public class HomeController : Controller
    {
        string constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;

        [AllowAnonymous]
        public ActionResult Login(string message)
        {

            ViewBag.Message = message;
            if (Session["userId"] != null)
            {
                return RedirectToAction("Dashboard");
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginClass user)
        {
            string encryptpass = ModuleEn.AES_Encrypt(ModuleEn.DataEncrypt(user.userPass));
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string query = "SELECT UserId, UserCode, UserName, UserPassword, UserRole " +
                               "FROM m_user " +
                               "WHERE UserCode = '" + user.userCode + "' AND UserPassword = '" + encryptpass + "'";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Session["userId"] = Convert.ToDouble(sdr["UserId"].ToString());
                            Session["userCode"] = sdr["UserCode"].ToString();
                            Session["userName"] = sdr["UserName"].ToString();
                            Session["UserRole"] = sdr["UserRole"].ToString();
                        }
                    }
                    con.Close();
                }
            }
            if (Session["userId"] == null)
            {
                ViewBag.Message = "Wrong UserId or Password";
                return RedirectToAction("Login", new { message = ViewBag.Message });
            }
            else
            {
                return RedirectToAction("Dashboard", new { message = "Login Success" });
            }
        }

        public ActionResult Dashboard()
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login");
            }
            DashboardClass dashboardClass = new DashboardClass();
            dashboardClass.newClient = 0;
            dashboardClass.openOrders = 0;
            dashboardClass.cancelOrders = 0;
            dashboardClass.closeOrders = 0;
            List<NewCustomer> newCustomers = new List<NewCustomer>();
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string query = "SELECT a.CustomerCode, (select COUNT(b.SOCode) from t_salesorder b WHERE b.CustomerCode = a.CustomerCode) AS TotalSO " +
                               "FROM m_customer a " +
                               "WHERE a.IsActive = '1'";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            newCustomers.Add(new NewCustomer
                            {
                                customerCode = sdr["CustomerCode"].ToString(),
                                totalSO = Convert.ToInt32(sdr["TotalSO"].ToString())
                            });
                        }
                    }
                    con.Close();
                }
                for (int i = 0; i < newCustomers.Count; i++)
                {
                    if (newCustomers[i].totalSO == 0)
                    {
                        dashboardClass.newClient = dashboardClass.newClient + 1;
                    }
                }
                string queryGetOpenSO = "SELECT COUNT(SOCode) AS TotalSO " +
                                        "FROM t_salesorder " +
                                        "WHERE Status = 0";
                using (MySqlCommand cmd = new MySqlCommand(queryGetOpenSO))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            dashboardClass.openOrders = Convert.ToInt32(sdr["TotalSO"].ToString());
                        }
                    }
                    con.Close();
                }
                string queryGetCancelSO = "SELECT COUNT(SOCode) AS TotalSO " +
                                        "FROM t_salesorder " +
                                        "WHERE Status = 2";
                using (MySqlCommand cmd = new MySqlCommand(queryGetCancelSO))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            dashboardClass.cancelOrders = Convert.ToInt32(sdr["TotalSO"].ToString());
                        }
                    }
                    con.Close();
                }
                string queryGetCloseSO = "SELECT COUNT(SOCode) AS TotalSO " +
                                        "FROM t_salesorder " +
                                        "WHERE Status = 1";
                using (MySqlCommand cmd = new MySqlCommand(queryGetCloseSO))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            dashboardClass.closeOrders = Convert.ToInt32(sdr["TotalSO"].ToString());
                        }
                    }
                    con.Close();
                }
            }
            return View(dashboardClass);
        }

        public ActionResult Logout()
        {
            Session["userId"] = null;
            Session.RemoveAll();
            return RedirectToAction("Login");
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}