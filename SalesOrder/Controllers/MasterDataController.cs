﻿using EnDecDll;
using MySql.Data.MySqlClient;
using SalesOrder.Models.MasterData;
using SalesOrder.Models.Transaction;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace SalesOrder.Controllers
{
    public class MasterDataController : Controller
    {

        string constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;

        public ActionResult ItemList()
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ItemListClass itemListClass = new ItemListClass();
            itemListClass.items = new List<ItemClass>();
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string query = "SELECT a.ItemId, a.ItemCode, a.ItemName, a.ItemPrice, a.ItemStock, b.CategoryCode, b.CategoryName, a.IsActive " +
                               "FROM m_item a " +
                               "JOIN m_category b ON b.CategoryCode = a.CategoryCode ";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            itemListClass.items.Add(new ItemClass
                            {
                                itemCode = sdr["ItemCode"].ToString(),
                                itemName = sdr["ItemName"].ToString(),
                                itemPrice = Convert.ToDouble(sdr["ItemPrice"].ToString()),
                                itemStock = Convert.ToInt32(sdr["ItemStock"].ToString()),
                                categoryCode = sdr["CategoryCode"].ToString(),
                                categoryName = sdr["CategoryName"].ToString(),
                                isActive = sdr["IsActive"].ToString() == "True" ? "Active" : "Inactive"
                            });
                        }
                    }
                    con.Close();
                }
            }
            return View(itemListClass);
        }

        public ActionResult CategoryList()
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            CategoryListClass categoryListClass = new CategoryListClass();
            categoryListClass.categories = new List<CategoryClass>();
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string query = "SELECT a.CategoryCode, a.CategoryName, a.IsActive, (SELECT COUNT(*) FROM m_item b WHERE b.CategoryCode = a.CategoryCode) AS total " +
                               "FROM m_category a ";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            categoryListClass.categories.Add(new CategoryClass
                            {
                                categoryCode = sdr["CategoryCode"].ToString(),
                                categoryName = sdr["CategoryName"].ToString(),
                                totalItem = Convert.ToInt32(sdr["total"].ToString()),
                                isActive = sdr["IsActive"].ToString() == "True" ? "Active" : "Inactive"
                            });
                        }
                    }
                    con.Close();
                }
            }
            return View(categoryListClass);
        }

        public ActionResult CustomerList()
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            CustomerListClass customerListClass = new CustomerListClass();
            customerListClass.customers = new List<CustomerClass>();
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string query = "SELECT a.CustomerId, a.CustomerCode, a.CustomerName, a.CustomerPhone, a.CustomerEmail, a.ContactPerson, a.CreditLimit, a.IsActive " +
                               "FROM m_customer a ";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            customerListClass.customers.Add(new CustomerClass
                            {
                                customerCode = sdr["CustomerCode"].ToString(),
                                customerName = sdr["CustomerName"].ToString(),
                                customerPhone = sdr["CustomerPhone"].ToString(),
                                customerEmail = sdr["CustomerEmail"].ToString(),
                                contactPerson = sdr["ContactPerson"].ToString(),
                                creditLimit = Convert.ToDouble(sdr["CreditLimit"].ToString()),
                                isActive = sdr["IsActive"].ToString() == "True" ? "Active" : "Inactive"
                            });
                        }
                    }
                    con.Close();
                }
            }
            return View(customerListClass);
        }

        public ActionResult UserList()
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            UserListClass userListClass = new UserListClass();
            userListClass.users = new List<UserClass>();
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string query = "SELECT a.UserId, a.UserCode, a.UserName, a.UserRole, a.UserPhone, a.UserEmail, a.UserAddress, a.UserCity, a.IsActive " +
                               "FROM m_user a " + 
                               "WHERE a.UserRole != 'SA'";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            userListClass.users.Add(new UserClass
                            {
                                userCode = sdr["UserCode"].ToString(),
                                userName = sdr["UserName"].ToString(),
                                userRole = sdr["UserRole"].ToString(),
                                userPhone = sdr["UserPhone"].ToString(),
                                userEmail = sdr["UserEmail"].ToString(),
                                userAddress = sdr["UserAddress"].ToString(),
                                userCity = sdr["UserCity"].ToString(),
                                isActive = sdr["IsActive"].ToString() == "True" ? "Active" : "Inactive"
                            });
                        }
                    }
                    con.Close();
                }
            }
            return View(userListClass);
        }

        public ActionResult AddItem()
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            AddItemClass addItemClass = new AddItemClass();
            addItemClass.categories = new List<CategoryClass>();
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string queryGetCategory = "SELECT CategoryCode, CategoryName " +
                                          "FROM m_category ";
                using (MySqlCommand cmd = new MySqlCommand(queryGetCategory))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            addItemClass.categories.Add(new CategoryClass
                            {
                                categoryCode = sdr["CategoryCode"].ToString(),
                                categoryName = sdr["CategoryName"].ToString(),
                            });
                        }
                    }
                    con.Close();
                }
            }
            return View(addItemClass);
        }

        public ActionResult AddCategory()
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            AddCategoryClass addCategoryClass = new AddCategoryClass();
            return View(addCategoryClass);
        }

        public ActionResult AddCustomer()
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            AddCustomerClass addCustomerClass = new AddCustomerClass();
            return View(addCustomerClass);
        }

        public ActionResult AddUser()
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            AddUserClass addUserClass = new AddUserClass();
            return View(addUserClass);
        }

        public ActionResult DetailsItem(string itemCode)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            DetailsItemClass detailsItemClass = new DetailsItemClass();
            detailsItemClass.categories = new List<CategoryClass>();
            string constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string queryGetItem = "SELECT a.ItemCode, a.ItemName, a.ItemPrice, a.ItemStock, b.CategoryCode, b.CategoryName, a.IsActive " +
                               "FROM m_item a " +
                               "JOIN m_category b ON b.CategoryCode = a.CategoryCode " +
                               "WHERE a.ItemCode = '" + itemCode + "'";
                using (MySqlCommand cmd = new MySqlCommand(queryGetItem))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            detailsItemClass.itemCode = sdr["ItemCode"].ToString();
                            detailsItemClass.itemName = sdr["ItemName"].ToString();
                            detailsItemClass.itemPrice = Convert.ToDouble(sdr["ItemPrice"].ToString());
                            detailsItemClass.itemStock = Convert.ToInt32(sdr["ItemStock"].ToString());
                            detailsItemClass.itemPriceStr = Convert.ToDecimal(detailsItemClass.itemPrice).ToString("#,###");
                            detailsItemClass.categoryCode = sdr["CategoryCode"].ToString();
                            detailsItemClass.categoryName = sdr["CategoryName"].ToString();
                            detailsItemClass.isActive = sdr["IsActive"].ToString();
                        }
                    }
                    con.Close();
                }
                string queryGetCategory = "SELECT CategoryCode, CategoryName " +
                                          "FROM m_category ";
                using (MySqlCommand cmd = new MySqlCommand(queryGetCategory))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            detailsItemClass.categories.Add(new CategoryClass
                            {
                                categoryCode = sdr["CategoryCode"].ToString(),
                                categoryName = sdr["CategoryName"].ToString(),
                            });
                        }
                    }
                    con.Close();
                }
            }
            return View(detailsItemClass);
        }

        public ActionResult DetailsCategory(string categoryCode)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            DetailsCategoryClass detailsCategoryClass = new DetailsCategoryClass();
            detailsCategoryClass.items = new List<ItemClass>();
            string constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string queryGetItem = "SELECT a.ItemCode, a.ItemName, a.ItemPrice, a.ItemStock, b.CategoryCode, b.CategoryName, a.IsActive " +
                               "FROM m_item a " +
                               "JOIN m_category b ON b.CategoryCode = a.CategoryCode " +
                               "WHERE a.CategoryCode = '" + categoryCode + "'";
                using (MySqlCommand cmd = new MySqlCommand(queryGetItem))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            detailsCategoryClass.items.Add(new ItemClass
                            {
                                itemCode = sdr["ItemCode"].ToString(),
                                itemName = sdr["ItemName"].ToString(),
                                itemPrice = Convert.ToDouble(sdr["ItemPrice"].ToString()),
                                itemStock = Convert.ToInt32(sdr["ItemStock"].ToString()),
                                itemPriceStr = Convert.ToDecimal(Convert.ToInt32(sdr["ItemPrice"].ToString())).ToString("#,###"),
                                categoryCode = sdr["CategoryCode"].ToString(),
                                categoryName = sdr["CategoryName"].ToString(),
                                isActive = sdr["IsActive"].ToString() == "True" ? "Active" : "Inactive"
                            });
                        }
                    }
                    con.Close();
                }
                string queryGetCategory = "SELECT CategoryCode, CategoryName, IsActive " +
                                          "FROM m_category " +
                                          "WHERE CategoryCode = '" + categoryCode + "'";
                using (MySqlCommand cmd = new MySqlCommand(queryGetCategory))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            detailsCategoryClass.categoryCode = sdr["CategoryCode"].ToString();
                            detailsCategoryClass.categoryName = sdr["CategoryName"].ToString();
                            detailsCategoryClass.isActive = sdr["IsActive"].ToString() == "True" ? "Active" : "Inactive";
                        }
                    }
                    con.Close();
                }
            }
            return View(detailsCategoryClass);
        }

        public ActionResult DetailsCustomer(string customerCode)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            DetailsCustomerClass detailsCategoryClass = new DetailsCustomerClass();
            detailsCategoryClass.soes = new List<SalesOrderClass>();
            string constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string queryGetItem = "SELECT a.SOCode, a.PostingDate, a.DocumentDate, a.DeliveryDate, a.Notes, a.Status, b.StringName " +
                               "FROM t_salesorder a " +
                               "JOIN m_stringmap b ON b.StringId = a.Status " +
                               "WHERE a.CustomerCode = '" + customerCode + "' AND a.Status = 0";
                using (MySqlCommand cmd = new MySqlCommand(queryGetItem))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            detailsCategoryClass.soes.Add(new SalesOrderClass
                            {
                                soCode = sdr["SOCode"].ToString(),
                                postingDate = ((DateTime)sdr["PostingDate"]).ToString("dd-MM-yyyy"),
                                documentDate = ((DateTime)sdr["DocumentDate"]).ToString("dd-MM-yyyy"),
                                deliveryDate = ((DateTime)sdr["DeliveryDate"]).ToString("dd-MM-yyyy"),
                                notes = sdr["Notes"].ToString(),
                                status = sdr["StringName"].ToString()
                            });
                        }
                    }
                    con.Close();
                }
                string queryGetCustomer = "SELECT CustomerCode, CustomerName, CustomerPhone, CustomerEmail, ContactPerson, CustomerCity, CreditLimit, IsActive " +
                                          "FROM m_customer " +
                                          "WHERE CustomerCode = '" + customerCode + "'";
                using (MySqlCommand cmd = new MySqlCommand(queryGetCustomer))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            detailsCategoryClass.customerCode = sdr["CustomerCode"].ToString();
                            detailsCategoryClass.customerName = sdr["CustomerName"].ToString();
                            detailsCategoryClass.customerPhone = sdr["CustomerPhone"].ToString();
                            detailsCategoryClass.customerEmail = sdr["CustomerEmail"].ToString();
                            detailsCategoryClass.contactPerson = sdr["ContactPerson"].ToString();
                            detailsCategoryClass.customerCity = sdr["CustomerCity"].ToString();
                            detailsCategoryClass.creditLimit = Convert.ToDouble(sdr["CreditLimit"].ToString());
                            detailsCategoryClass.isActive = sdr["IsActive"].ToString() == "True" ? "Active" : "Inactive";
                        }
                    }
                    con.Close();
                }
            }
            return View(detailsCategoryClass);
        }

        public ActionResult DetailsUser(string userCode)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            DetailsUserClass detailsUserClass = new DetailsUserClass();
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string queryGetUser = "SELECT UserCode, UserName, UserRole, UserPhone, UserEmail, UserAddress, UserCity, IsActive " +
                                          "FROM m_user " +
                                          "WHERE UserCode = '" + userCode + "'";
                using (MySqlCommand cmd = new MySqlCommand(queryGetUser))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            detailsUserClass.userCode = sdr["UserCode"].ToString();
                            detailsUserClass.userName = sdr["UserName"].ToString();
                            detailsUserClass.userPhone = sdr["UserPhone"].ToString();
                            detailsUserClass.userEmail = sdr["UserEmail"].ToString();
                            detailsUserClass.userAddress = sdr["UserAddress"].ToString();
                            detailsUserClass.userCity = sdr["UserCity"].ToString();
                            detailsUserClass.userRole = sdr["UserRole"].ToString();
                            detailsUserClass.isActive = sdr["IsActive"].ToString() == "True" ? "Active" : "Inactive";
                        }
                    }
                    con.Close();
                }
            }
            return View(detailsUserClass);
        }

        [HttpPost]
        public ActionResult SubmitItem(AddItemClass items)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            double userId = long.Parse(Session["userId"].ToString());
            DateTime dt = DateTime.Now;
            var txOptions = new TransactionOptions();
            txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            string lastCode = "";
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
            {
                try
                {
                    using (MySqlConnection con = new MySqlConnection(constr))
                    {
                        string queryGet = "SELECT ItemCode " +
                               "FROM m_item " +
                               "WHERE CategoryCode = '" + items.categoryCode + "'" +
                               "ORDER BY ItemCode ASC";
                        using (MySqlCommand command = new MySqlCommand(queryGet))
                        {
                            command.Connection = con;
                            con.Open();
                            using (MySqlDataReader sdr = command.ExecuteReader())
                            {
                                while (sdr.Read())
                                {
                                    lastCode = sdr["ItemCode"].ToString();
                                }
                            }
                            con.Close();
                        }
                        if (lastCode == "")
                        {
                            lastCode = items.categoryCode + "-00001";
                        }
                        else
                        {
                            string[] splitCode = lastCode.Split('-');
                            lastCode = items.categoryCode + "-" + (Int32.Parse(splitCode[1]) + 1).ToString().PadLeft(5, '0');
                        }
                        string queryInsert = "INSERT INTO m_item(ItemCode, ItemName, CategoryCode, ItemPrice, ItemStock, IsActive, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy) " +
                                       "VALUES('" + lastCode + "', '" + items.itemName + "', '" + items.categoryCode + "', " + items.itemPrice + ", " + items.itemStock + ", '1', '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "', " + userId + ", '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "', " + userId + ")";
                        using (MySqlCommand cmd = new MySqlCommand(queryInsert))
                        {
                            cmd.Connection = con;
                            con.Open();
                            using (MySqlDataReader sdr = cmd.ExecuteReader())
                            {
                                while (sdr.Read())
                                {

                                }
                            }
                            con.Close();
                        }
                    }
                    transaction.Complete();
                    transaction.Dispose();
                }
                catch (Exception ex)
                {
                    transaction.Dispose();
                }
            }
            return RedirectToAction("ItemList", "MasterData");
        }

        [HttpPost]
        public ActionResult SubmitCategory(AddCategoryClass categories)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            double userId = long.Parse(Session["userId"].ToString());
            DateTime dt = DateTime.Now;
            var txOptions = new TransactionOptions();
            txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
            {
                try
                {
                    using (MySqlConnection con = new MySqlConnection(constr))
                    {
                        string queryInsert = "INSERT INTO m_category(CategoryCode, CategoryName, IsActive, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy) " +
                                       "VALUES('" + categories.categoryCode + "', '" + categories.categoryName + "', '1', '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "', " + userId + ", '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "', " + userId + ")";
                        using (MySqlCommand cmd = new MySqlCommand(queryInsert))
                        {
                            cmd.Connection = con;
                            con.Open();
                            using (MySqlDataReader sdr = cmd.ExecuteReader())
                            {
                                while (sdr.Read())
                                {

                                }
                            }
                            con.Close();
                        }
                    }
                    transaction.Complete();
                    transaction.Dispose();
                }
                catch (Exception ex)
                {
                    transaction.Dispose();
                }
            }
            return RedirectToAction("CategoryList", "MasterData");
        }

        [HttpPost]
        public ActionResult SubmitCustomer(AddCustomerClass customers)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            double userId = long.Parse(Session["userId"].ToString());
            DateTime dt = DateTime.Now;
            var txOptions = new TransactionOptions();
            txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            string lastCode = "";
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
            {
                try
                {
                    using (MySqlConnection con = new MySqlConnection(constr))
                    {
                        string queryGet = "SELECT CustomerCode " +
                               "FROM m_customer " +
                               "WHERE CustomerCity = '" + customers.customerCity + "'" +
                               "ORDER BY CustomerCode ASC";
                        using (MySqlCommand command = new MySqlCommand(queryGet))
                        {
                            command.Connection = con;
                            con.Open();
                            using (MySqlDataReader sdr = command.ExecuteReader())
                            {
                                while (sdr.Read())
                                {
                                    lastCode = sdr["CustomerCode"].ToString();
                                }
                            }
                            con.Close();
                        }
                        if (lastCode == "")
                        {
                            lastCode = customers.customerCity.ToUpper().Substring(0,3) + "-00001";
                        }
                        else
                        {
                            string[] splitCode = lastCode.Split('-');
                            lastCode = customers.customerCity.ToUpper().Substring(0, 3) + "-" + (Int32.Parse(splitCode[1]) + 1).ToString().PadLeft(5, '0');
                        }
                        string queryInsert = "INSERT INTO m_customer(CustomerCode, CustomerName, CustomerPhone, CustomerEmail, ContactPerson, CustomerCity, CreditLimit, IsActive, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy) " +
                                       "VALUES('" + lastCode + "', '" + customers.customerName + "', '" + customers.customerPhone + "', '" + customers.customerEmail +
                                       "', '" + customers.contactPerson + "', '" + customers.customerCity + "', " + customers.creditLimit + ", '1', '" + 
                                       dt.ToString("yyyy-MM-dd HH:mm:ss") + "', " + userId + ", '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "', " + userId + ")";
                        using (MySqlCommand cmd = new MySqlCommand(queryInsert))
                        {
                            cmd.Connection = con;
                            con.Open();
                            using (MySqlDataReader sdr = cmd.ExecuteReader())
                            {
                                while (sdr.Read())
                                {

                                }
                            }
                            con.Close();
                        }
                    }
                    transaction.Complete();
                    transaction.Dispose();
                }
                catch (Exception ex)
                {
                    transaction.Dispose();
                }
            }
            return RedirectToAction("CustomerList", "MasterData");
        }

        [HttpPost]
        public ActionResult SubmitUser(AddUserClass users)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            double userId = long.Parse(Session["userId"].ToString());
            DateTime dt = DateTime.Now;
            var txOptions = new TransactionOptions();
            txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            string lastCode = "";
            string encryptpass = ModuleEn.AES_Encrypt(ModuleEn.DataEncrypt("1234"));
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
            {
                try
                {
                    using (MySqlConnection con = new MySqlConnection(constr))
                    {
                        string queryGet = "SELECT UserCode " +
                               "FROM m_user " +
                               "WHERE UserCity = '" + users.userCity + "'" +
                               "ORDER BY UserCode ASC";
                        using (MySqlCommand command = new MySqlCommand(queryGet))
                        {
                            command.Connection = con;
                            con.Open();
                            using (MySqlDataReader sdr = command.ExecuteReader())
                            {
                                while (sdr.Read())
                                {
                                    lastCode = sdr["UserCode"].ToString();
                                }
                            }
                            con.Close();
                        }
                        if (lastCode == "")
                        {
                            lastCode = users.userCity.ToUpper().Substring(0, 3) + "-00001";
                        }
                        else
                        {
                            string[] splitCode = lastCode.Split('-');
                            lastCode = users.userCity.ToUpper().Substring(0, 3) + "-" + (Int32.Parse(splitCode[1]) + 1).ToString().PadLeft(5, '0');
                        }
                        string queryInsert = "INSERT INTO m_user(UserCode, UserName, UserPassword, UserRole, UserPhone, UserEmail, UserAddress, UserCity, IsActive, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy) " +
                                       "VALUES('" + lastCode + "', '" + users.userName + "', '" + encryptpass + "', '" + users.userRole +
                                       "', '" + users.userPhone + "', '" + users.userEmail + "', '" + users.userAddress + "', '" + users.userCity + "', '1', '" +
                                       dt.ToString("yyyy-MM-dd HH:mm:ss") + "', " + userId + ", '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "', " + userId + ")";
                        using (MySqlCommand cmd = new MySqlCommand(queryInsert))
                        {
                            cmd.Connection = con;
                            con.Open();
                            using (MySqlDataReader sdr = cmd.ExecuteReader())
                            {
                                while (sdr.Read())
                                {

                                }
                            }
                            con.Close();
                        }
                    }
                    transaction.Complete();
                    transaction.Dispose();
                }
                catch (Exception ex)
                {
                    transaction.Dispose();
                }
            }
            return RedirectToAction("UserList", "MasterData");
        }

        [HttpPost]
        public ActionResult UpdateItem(DetailsItemClass items)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            double userId = long.Parse(Session["userId"].ToString());
            DateTime dt = DateTime.Now;
            var txOptions = new TransactionOptions();
            txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
            {
                try
                {
                    using (MySqlConnection con = new MySqlConnection(constr))
                    {
                        string queryInsert = "UPDATE m_item SET ItemName = '" + items.itemName + "', CategoryCode = '" + items.categoryCode +
                                             "', ItemPrice = " + items.itemPrice +
                                             ", ModifiedOn = '" + dt.ToString("yyyy-MM-dd HH:mm:ss") +
                                             "', ModifiedBy = " + userId + " WHERE ItemCode = '" + items.itemCode + "'";
                        using (MySqlCommand cmd = new MySqlCommand(queryInsert))
                        {
                            cmd.Connection = con;
                            con.Open();
                            using (MySqlDataReader sdr = cmd.ExecuteReader())
                            {
                                while (sdr.Read())
                                {

                                }
                            }
                            con.Close();
                        }
                    }
                    transaction.Complete();
                    transaction.Dispose();
                }
                catch (Exception ex)
                {
                    transaction.Dispose();
                }
            }
            return RedirectToAction("ItemList", "MasterData");
        }

        [HttpPost]
        public ActionResult UpdateCategory(DetailsCategoryClass categories)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            double userId = long.Parse(Session["userId"].ToString());
            DateTime dt = DateTime.Now;
            var txOptions = new TransactionOptions();
            txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
            {
                try
                {
                    using (MySqlConnection con = new MySqlConnection(constr))
                    {
                        string queryInsert = "UPDATE m_category SET CategoryName = '" + categories.categoryName + "', ModifiedOn = '" + dt.ToString("yyyy-MM-dd HH:mm:ss") +
                                             "', ModifiedBy = " + userId + " WHERE CategoryCode = '" + categories.categoryCode + "'";
                        using (MySqlCommand cmd = new MySqlCommand(queryInsert))
                        {
                            cmd.Connection = con;
                            con.Open();
                            using (MySqlDataReader sdr = cmd.ExecuteReader())
                            {
                                while (sdr.Read())
                                {

                                }
                            }
                            con.Close();
                        }
                    }
                    transaction.Complete();
                    transaction.Dispose();
                }
                catch (Exception ex)
                {
                    transaction.Dispose();
                }
            }
            return RedirectToAction("CategoryList", "MasterData");
        }

        [HttpPost]
        public ActionResult UpdateCustomer(DetailsCustomerClass customers)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            double userId = long.Parse(Session["userId"].ToString());
            DateTime dt = DateTime.Now;
            var txOptions = new TransactionOptions();
            txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
            {
                try
                {
                    using (MySqlConnection con = new MySqlConnection(constr))
                    {
                        string queryInsert = "UPDATE m_customer SET CustomerName = '" + customers.customerName + 
                                             "', CustomerPhone = '" + customers.customerPhone +
                                             "', CustomerEmail = '" + customers.customerEmail + 
                                             "', ContactPerson = '" + customers.contactPerson +
                                             "', CustomerCity = '" + customers.customerCity +
                                             "', CreditLimit = " + customers.creditLimit + 
                                             ", ModifiedOn = '" + dt.ToString("yyyy-MM-dd HH:mm:ss") +
                                             "', ModifiedBy = " + userId + " WHERE CustomerCode = '" + customers.customerCode + "'";
                        using (MySqlCommand cmd = new MySqlCommand(queryInsert))
                        {
                            cmd.Connection = con;
                            con.Open();
                            using (MySqlDataReader sdr = cmd.ExecuteReader())
                            {
                                while (sdr.Read())
                                {

                                }
                            }
                            con.Close();
                        }
                    }
                    transaction.Complete();
                    transaction.Dispose();
                }
                catch (Exception ex)
                {
                    transaction.Dispose();
                }
            }
            return RedirectToAction("CustomerList", "MasterData");
        }

        [HttpPost]
        public ActionResult UpdateUser(DetailsUserClass users)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            double userId = long.Parse(Session["userId"].ToString());
            DateTime dt = DateTime.Now;
            var txOptions = new TransactionOptions();
            txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
            {
                try
                {
                    using (MySqlConnection con = new MySqlConnection(constr))
                    {
                        string queryInsert = "UPDATE m_user SET UserName = '" + users.userName +
                                             "', UserRole = '" + users.userRole +
                                             "', UserPhone = '" + users.userPhone +
                                             "', UserEmail = '" + users.userEmail +
                                             "', UserAddress = '" + users.userAddress +
                                             "', UserCity = '" + users.userCity +
                                             "', ModifiedOn = '" + dt.ToString("yyyy-MM-dd HH:mm:ss") +
                                             "', ModifiedBy = " + userId + " WHERE UserCode = '" + users.userCode + "'";
                        using (MySqlCommand cmd = new MySqlCommand(queryInsert))
                        {
                            cmd.Connection = con;
                            con.Open();
                            using (MySqlDataReader sdr = cmd.ExecuteReader())
                            {
                                while (sdr.Read())
                                {

                                }
                            }
                            con.Close();
                        }
                    }
                    transaction.Complete();
                    transaction.Dispose();
                }
                catch (Exception ex)
                {
                    transaction.Dispose();
                }
            }
            return RedirectToAction("UserList", "MasterData");
        }

        public ActionResult UpdateStatusItem(string id, string status)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            bool isValid = true;
            string messages = "";
            double userId = long.Parse(Session["userId"].ToString());
            DateTime dt = DateTime.Now;
            var txOptions = new TransactionOptions();
            txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
            {
                try
                {
                    using (MySqlConnection con = new MySqlConnection(constr))
                    {
                        string query = "UPDATE m_item SET isActive = '" + status + "', ModifiedBy = " + userId +
                            ", ModifiedOn = '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "' WHERE ItemCode = '" + id + "'";
                        MySqlCommand cmd = new MySqlCommand(query);
                        cmd.Connection = con;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        con.Close();
                    }
                    transaction.Complete();
                    transaction.Dispose();
                    messages = "Delete success";
                }
                catch (Exception ex)
                {
                    messages = ex.Message;
                    messages = "Error Failed To update item";
                    transaction.Dispose();
                    isValid = false;
                }
            }
            var obj = new
            {
                valid = isValid,
                pesansingkat = messages
            };
            return Json(obj);
        }

        public ActionResult UpdateStatusCategory(string id, string status)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            bool isValid = true;
            string messages = "";
            double userId = long.Parse(Session["userId"].ToString());
            DateTime dt = DateTime.Now;
            var txOptions = new TransactionOptions();
            txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
            {
                try
                {
                    using (MySqlConnection con = new MySqlConnection(constr))
                    {
                        string query = "UPDATE m_category SET isActive = '" + status + "', ModifiedBy = " + userId +
                            ", ModifiedOn = '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "' WHERE CategoryCode = '" + id + "'";
                        MySqlCommand cmd = new MySqlCommand(query);
                        cmd.Connection = con;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        con.Close();
                    }
                    transaction.Complete();
                    transaction.Dispose();
                    messages = "Delete success";
                }
                catch (Exception ex)
                {
                    messages = ex.Message;
                    messages = "Error Failed To update category";
                    transaction.Dispose();
                    isValid = false;
                }
            }
            var obj = new
            {
                valid = isValid,
                pesansingkat = messages
            };
            return Json(obj);
        }

        public ActionResult UpdateStatusCustomer(string id, string status)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            bool isValid = true;
            string messages = "";
            double userId = long.Parse(Session["userId"].ToString());
            DateTime dt = DateTime.Now;
            var txOptions = new TransactionOptions();
            txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
            {
                try
                {
                    using (MySqlConnection con = new MySqlConnection(constr))
                    {
                        string query = "UPDATE m_customer SET IsActive = '" + status + "', ModifiedBy = " + userId +
                            ", ModifiedOn = '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "' WHERE CustomerCode = '" + id + "'";
                        MySqlCommand cmd = new MySqlCommand(query);
                        cmd.Connection = con;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        con.Close();
                    }
                    transaction.Complete();
                    transaction.Dispose();
                    messages = "Delete success";
                }
                catch (Exception ex)
                {
                    messages = ex.Message;
                    messages = "Error Failed To update customer";
                    transaction.Dispose();
                    isValid = false;
                }
            }
            var obj = new
            {
                valid = isValid,
                pesansingkat = messages
            };
            return Json(obj);
        }

        public ActionResult UpdateStatusUser(string id, string status)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            bool isValid = true;
            string messages = "";
            double userId = long.Parse(Session["userId"].ToString());
            DateTime dt = DateTime.Now;
            var txOptions = new TransactionOptions();
            txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
            {
                try
                {
                    using (MySqlConnection con = new MySqlConnection(constr))
                    {
                        string query = "UPDATE m_user SET IsActive = '" + status + "', ModifiedBy = " + userId +
                            ", ModifiedOn = '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "' WHERE UserCode = '" + id + "'";
                        MySqlCommand cmd = new MySqlCommand(query);
                        cmd.Connection = con;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        con.Close();
                    }
                    transaction.Complete();
                    transaction.Dispose();
                    messages = "Delete success";
                }
                catch (Exception ex)
                {
                    messages = ex.Message;
                    messages = "Error Failed To update user";
                    transaction.Dispose();
                    isValid = false;
                }
            }
            var obj = new
            {
                valid = isValid,
                pesansingkat = messages
            };
            return Json(obj);
        }
    }
}