﻿using MySql.Data.MySqlClient;
using SalesOrder.Models.Report;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesOrder.Controllers
{
    public class ReportController : Controller
    {
        string constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;

        public ActionResult SalesOrderDetail()
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            SODetailClass sODetailClass = new SODetailClass();
            sODetailClass.statues = new List<GetStatusClass>();
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string query = "SELECT a.StringId, a.StringName " +
                                "FROM m_stringmap a ";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            sODetailClass.statues.Add(new GetStatusClass
                            {
                                statusCode = sdr["StringId"].ToString(),
                                statusName = sdr["StringName"].ToString()
                            });
                        }
                    }
                    con.Close();
                }
            }
            return View(sODetailClass);
        }

        public JsonResult GetSalesOrderDetail(string from, string to, string status)
        {
            long userId = long.Parse(Session["userId"].ToString());
            from = DateTime.ParseExact(from, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            to = DateTime.ParseExact(to, "dd-MM-yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            List<GetReportSalesOrderDetail> getReportSalesOrderDetails = new List<GetReportSalesOrderDetail>();
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string query = "SELECT a.SOCode, a.PostingDate, a.DocumentDate, a.DeliveryDate, a.CustomerCode, a.CustomerName, a.Notes, a.Status" +
                                ", b.ItemCode, b.ItemName, b.ItemQty, b.ItemPrice, c.StringName " +
                                "FROM t_salesorder a " +
                                "JOIN t_salesorderdetail b ON b.SOCode = a.SOCode " +
                                "JOIN m_stringmap c ON c.StringId = a.Status " + 
                                "WHERE a.CreatedBy = " + userId + " AND a.PostingDate BETWEEN '" + from + "' AND '" + to + "'";
                if (status != "-1")
                {
                    query = "SELECT a.SOCode, a.PostingDate, a.DocumentDate, a.DeliveryDate, a.CustomerCode, a.CustomerName, a.Notes, a.Status" +
                                ", b.ItemCode, b.ItemName, b.ItemQty, b.ItemPrice, c.StringName " +
                                "FROM t_salesorder a " +
                                "JOIN t_salesorderdetail b ON b.SOCode = a.SOCode " +
                                "JOIN m_stringmap c ON c.StringId = a.Status " +
                                "WHERE a.CreatedBy = " + userId + " AND a.Status = " + status + " AND a.PostingDate BETWEEN '" + from + "' AND '" + to + "'";
                }
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            getReportSalesOrderDetails.Add(new GetReportSalesOrderDetail
                            {
                                SOCode = sdr["SOCode"].ToString(),
                                postingDate = ((DateTime)sdr["PostingDate"]).ToString("dd-MM-yyyy"),
                                documentDate = ((DateTime)sdr["DocumentDate"]).ToString("dd-MM-yyyy"),
                                deliveryDate = ((DateTime)sdr["DeliveryDate"]).ToString("dd-MM-yyyy"),
                                customerCode = sdr["CustomerCode"].ToString(),
                                customerName = sdr["CustomerName"].ToString(),
                                notes = sdr["Notes"].ToString(),
                                status = sdr["StringName"].ToString(),
                                itemCode = sdr["ItemCode"].ToString(),
                                itemName = sdr["ItemName"].ToString(),
                                itemQty = Convert.ToInt32(sdr["ItemQty"].ToString()),
                                itemPrice = Convert.ToDouble(sdr["ItemPrice"].ToString()),
                                strItemPrice = Convert.ToDecimal(Convert.ToDouble(sdr["ItemPrice"].ToString())).ToString("#,###")
                            });
                        }
                    }
                    con.Close();
                }
                return Json(getReportSalesOrderDetails, JsonRequestBehavior.AllowGet);
            }
        }

        public string ConvertDateTimeFormat(string input, string inputFormat, string outputFormat, IFormatProvider culture)
        {
            DateTime dateTime = DateTime.ParseExact(input, inputFormat, culture);
            return dateTime.ToString(outputFormat, culture);
        }
    }
}