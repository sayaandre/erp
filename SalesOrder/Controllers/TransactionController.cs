﻿using MySql.Data.MySqlClient;
using SalesOrder.Models.MasterData;
using SalesOrder.Models.Transaction;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace SalesOrder.Controllers
{
    public class TransactionController : Controller
    {

        string constr = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;

        public ActionResult ListSalesOrder()
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            double userId = long.Parse(Session["userId"].ToString());
            ListSalesOrderClass listSalesOrderClass = new ListSalesOrderClass();
            listSalesOrderClass.soes = new List<SalesOrderClass>();
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string query = "SELECT a.SOCode, a.PostingDate, a.DocumentDate, a.DeliveryDate, a.CustomerCode, a.CustomerName, a.Notes, a.Status, b.StringName " +
                               "FROM t_salesorder a " +
                               "JOIN m_stringmap b ON b.StringId = a.Status " + 
                               "WHERE a.CreatedBy = " + userId + " " +
                               "ORDER BY a.SOCode DESC";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            listSalesOrderClass.soes.Add(new SalesOrderClass
                            {
                                soCode = sdr["SOCode"].ToString(),
                                postingDate = ((DateTime)sdr["PostingDate"]).ToString("dd-MM-yyyy"),
                                documentDate = sdr["DocumentDate"].ToString().Length > 0 ? ((DateTime)sdr["DocumentDate"]).ToString("dd-MM-yyyy") : "-",
                                deliveryDate = sdr["DeliveryDate"].ToString().Length > 0 ? ((DateTime)sdr["DeliveryDate"]).ToString("dd-MM-yyyy") : "-",
                                customerCode = sdr["CustomerCode"].ToString(),
                                customerName = sdr["CustomerName"].ToString(),
                                notes = sdr["Notes"].ToString(),
                                status = sdr["StringName"].ToString(),
                                totalItem = 0
                            });
                        }
                    }
                    con.Close();
                }
            }
            return View(listSalesOrderClass);
        }

        public ActionResult AddSalesOrder()
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            AddSalesOrderClass addSalesOrderClass = new AddSalesOrderClass();
            addSalesOrderClass.customers = new List<CustomerClass>();
            addSalesOrderClass.items = new List<ItemClass>();
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string queryGetCustomer = "SELECT a.CustomerCode, a.CustomerName, a.CustomerPhone, a.CustomerEmail, a.ContactPerson, a.CustomerCity, a.CreditLimit, " +
                                          "a.CreditLimit - (SELECT SUM(c.ItemQty * c.ItemPrice) " + 
                                                            "FROM t_salesorder b " + 
                                                            "JOIN t_salesorderdetail c ON c.SOCode = b.SOCode " + 
                                                            "WHERE b.CustomerCode = a.CustomerCode AND b.Status = 0) AS LastCredit " + 
                                          "FROM m_customer a " + 
                                          "WHERE a.IsActive = '1'";
                using (MySqlCommand cmd = new MySqlCommand(queryGetCustomer))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            addSalesOrderClass.customers.Add(new CustomerClass
                            {
                                customerCode = sdr["CustomerCode"].ToString(),
                                customerName = sdr["CustomerName"].ToString(),
                                customerPhone = sdr["CustomerPhone"].ToString(),
                                customerEmail = sdr["CustomerEmail"].ToString(),
                                contactPerson = sdr["ContactPerson"].ToString(),
                                customerCity = sdr["CustomerCity"].ToString(),
                                lastCreditLimit = Convert.ToDouble(sdr["LastCredit"].ToString().Length == 0 ? sdr["CreditLimit"].ToString() : sdr["LastCredit"].ToString()),
                                creditLimit = Convert.ToDouble(sdr["CreditLimit"].ToString())
                            });
                        }
                    }
                    con.Close();
                }
                string queryGetItem = "SELECT a.ItemCode, a.ItemName, a.ItemStock, a.ItemPrice, b.CategoryCode, b.CategoryName " +
                               "FROM m_item a " +
                               "JOIN m_category b ON b.CategoryCode = a.CategoryCode " +
                               "WHERE a.IsActive = '1' AND b.IsActive = '1'";
                using (MySqlCommand cmd = new MySqlCommand(queryGetItem))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            addSalesOrderClass.items.Add(new ItemClass
                            {
                                itemCode = sdr["ItemCode"].ToString(),
                                itemName = sdr["ItemName"].ToString(),
                                itemPrice = Convert.ToDouble(sdr["ItemPrice"].ToString()),
                                itemStock = Convert.ToInt32(sdr["ItemStock"].ToString()),
                                itemPriceStr = Convert.ToDecimal(Convert.ToDouble(sdr["ItemPrice"].ToString())).ToString("#,###"),
                                categoryCode = sdr["CategoryCode"].ToString(),
                                categoryName = sdr["CategoryName"].ToString()
                            });
                        }
                    }
                    con.Close();
                }
            }
            return View(addSalesOrderClass);
        }

        public ActionResult DetailsSalesOrder(string code)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            double total = 0;
            DetailsSalesOrderClass detailsSalesOrderClass = new DetailsSalesOrderClass();
            detailsSalesOrderClass.details = new List<SalesOrderDetailsClass>();
            detailsSalesOrderClass.customers = new List<CustomerClass>();
            detailsSalesOrderClass.items = new List<ItemClass>();
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string queryGetDetail = "SELECT a.ItemCode, a.ItemName, a.ItemQty, a.ItemPrice, a.SOCode, b.ItemStock, c.CategoryName " +
                               "FROM t_salesorderdetail a " +
                               "JOIN m_item b ON b.ItemCode = a.ItemCode " +
                               "JOIN m_category c ON c.CategoryCode = b.CategoryCode " +
                               "WHERE a.SOCode = '" + code + "'";
                using (MySqlCommand cmd = new MySqlCommand(queryGetDetail))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            detailsSalesOrderClass.details.Add(new SalesOrderDetailsClass
                            {
                                itemCode = sdr["ItemCode"].ToString(),
                                itemName = sdr["ItemName"].ToString(),
                                itemQty = Convert.ToInt32(sdr["ItemQty"].ToString()),
                                itemPrice = Convert.ToDouble(sdr["ItemPrice"].ToString()),
                                itemPriceStr = Convert.ToDecimal(Convert.ToDouble(sdr["ItemPrice"].ToString())).ToString("#,###"),
                                soCode = sdr["SOCode"].ToString(),
                                categoryName = sdr["CategoryName"].ToString(),
                                itemStock = Convert.ToInt32(sdr["ItemStock"].ToString()),
                            });
                            total = total + (Convert.ToInt32(sdr["ItemQty"].ToString()) * Convert.ToDouble(sdr["ItemPrice"].ToString()));
                        }
                    }
                    con.Close();
                }
                string queryGetSO = "SELECT a.SOCode, a.PostingDate, a.DocumentDate, a.DeliveryDate, b.CustomerCode, b.CustomerName, a.Notes, " + 
                                    "a.Status, b.CustomerPhone, b.CustomerEmail, b.ContactPerson, b.CustomerCity, b.CreditLimit, c.StringName, " +
                                    "b.CreditLimit - (SELECT SUM(c1.ItemQty * c1.ItemPrice) " +
                                                            "FROM t_salesorder b1 " +
                                                            "JOIN t_salesorderdetail c1 ON c1.SOCode = b1.SOCode " +
                                                            "WHERE b1.CustomerCode = a.CustomerCode AND b1.Status = 0) AS LastCredit " +
                                    "FROM t_salesorder a " +
                                    "JOIN m_customer b ON b.CustomerCode = a.CustomerCode " +
                                    "JOIN m_stringmap c ON c.StringId = a.Status " +
                                    "WHERE SOCode = '" + code + "'";
                using (MySqlCommand cmd = new MySqlCommand(queryGetSO))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            detailsSalesOrderClass.soCode = sdr["SOCode"].ToString();
                            detailsSalesOrderClass.postingDate = ((DateTime)sdr["PostingDate"]).ToString("dd-MM-yyyy");
                            detailsSalesOrderClass.documentDate = sdr["DocumentDate"].ToString().Length > 0 ? ((DateTime)sdr["DocumentDate"]).ToString("dd-MM-yyyy") : "-";
                            detailsSalesOrderClass.deliveryDate = sdr["DeliveryDate"].ToString().Length > 0 ? ((DateTime)sdr["DeliveryDate"]).ToString("dd-MM-yyyy") : "-";
                            detailsSalesOrderClass.customerCode = sdr["CustomerCode"].ToString();
                            detailsSalesOrderClass.customerName = sdr["CustomerName"].ToString();
                            detailsSalesOrderClass.notes = sdr["Notes"].ToString();
                            detailsSalesOrderClass.status = sdr["StringName"].ToString();
                            detailsSalesOrderClass.customerPhone = sdr["CustomerPhone"].ToString();
                            detailsSalesOrderClass.customerEmail = sdr["CustomerEmail"].ToString();
                            detailsSalesOrderClass.customerCity = sdr["CustomerCity"].ToString();
                            detailsSalesOrderClass.creditLimit = Convert.ToDouble(sdr["CreditLimit"].ToString());
                            detailsSalesOrderClass.strCreditLimit = Convert.ToDecimal(detailsSalesOrderClass.creditLimit).ToString("#,###");
                            detailsSalesOrderClass.lastCreditLimit = Convert.ToDouble(sdr["LastCredit"].ToString().Length == 0 ? sdr["CreditLimit"].ToString() : sdr["LastCredit"].ToString()) + total;
                            detailsSalesOrderClass.strLastCreditLimit = Convert.ToDecimal(detailsSalesOrderClass.lastCreditLimit).ToString("#,###");

                        }
                    }
                    con.Close();
                }
                string queryGetCustomer = "SELECT a.CustomerCode, a.CustomerName, a.CustomerPhone, a.CustomerEmail, a.ContactPerson, a.CustomerCity, a.CreditLimit, " +
                                          "a.CreditLimit - (SELECT SUM(c.ItemQty * c.ItemPrice) " +
                                                            "FROM t_salesorder b " +
                                                            "JOIN t_salesorderdetail c ON c.SOCode = b.SOCode " +
                                                            "WHERE b.CustomerCode = a.CustomerCode AND b.Status = 0) AS LastCredit " +
                                          "FROM m_customer a " +
                                          "WHERE a.IsActive = '1'";
                using (MySqlCommand cmd = new MySqlCommand(queryGetCustomer))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            detailsSalesOrderClass.customers.Add(new CustomerClass
                            {
                                customerCode = sdr["CustomerCode"].ToString(),
                                customerName = sdr["CustomerName"].ToString(),
                                customerPhone = sdr["CustomerPhone"].ToString(),
                                customerEmail = sdr["CustomerEmail"].ToString(),
                                contactPerson = sdr["ContactPerson"].ToString(),
                                customerCity = sdr["CustomerCity"].ToString(),
                                lastCreditLimit = Convert.ToDouble(sdr["LastCredit"].ToString().Length == 0 ? sdr["CreditLimit"].ToString() : sdr["LastCredit"].ToString()) + total,
                                creditLimit = Convert.ToDouble(sdr["CreditLimit"].ToString())

                            });
                        }
                    }
                    con.Close();
                }
                string queryGetItem = "SELECT a.ItemCode, a.ItemName, a.ItemStock, a.ItemPrice, b.CategoryCode, b.CategoryName " +
                               "FROM m_item a " +
                               "JOIN m_category b ON b.CategoryCode = a.CategoryCode " +
                               "WHERE a.IsActive = '1' AND b.IsActive = '1'";
                using (MySqlCommand cmd = new MySqlCommand(queryGetItem))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            detailsSalesOrderClass.items.Add(new ItemClass
                            {
                                itemCode = sdr["ItemCode"].ToString(),
                                itemName = sdr["ItemName"].ToString(),
                                itemPrice = Convert.ToDouble(sdr["ItemPrice"].ToString()),
                                itemStock = Convert.ToInt32(sdr["ItemStock"].ToString()),
                                itemPriceStr = Convert.ToDecimal(Convert.ToDouble(sdr["ItemPrice"].ToString())).ToString("#,###"),
                                categoryCode = sdr["CategoryCode"].ToString(),
                                categoryName = sdr["CategoryName"].ToString()
                            });
                        }
                    }
                    con.Close();
                }
            }
            return View(detailsSalesOrderClass);
        }

        public ActionResult SubmitSalesOrder(string postingDate, string documentDate, string deliveryDate, string notes, string customerCode, string customerName, string creditLimit, string items)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login");
            }
            bool isValid = true;
            string nofaktur = "";
            string messages = "";
            double total = 0;
            long userId = long.Parse(Session["userId"].ToString());
            DateTime dt = DateTime.Now;
            var txtOptions = new TransactionOptions();
            txtOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, txtOptions))
            {
                try
                {
                    using (MySqlConnection con = new MySqlConnection(constr))
                    {
                        string queryGet = "SELECT SOCode " +
                               "FROM t_salesorder " +
                               "WHERE CreatedOn BETWEEN '" + dt.ToString("yyyy-MM-dd") + " 00:00:00' AND '" + dt.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00'" +
                               "ORDER BY SOCode ASC";
                        using (MySqlCommand command = new MySqlCommand(queryGet))
                        {
                            command.Connection = con;
                            con.Open();
                            using (MySqlDataReader sdr = command.ExecuteReader())
                            {
                                while (sdr.Read())
                                {
                                    nofaktur = sdr["SOCode"].ToString();
                                }
                            }
                            con.Close();
                        }
                        if (nofaktur == "")
                        {
                            nofaktur = "SO-" + dt.ToString("yyMMdd") + "-00001";
                        }
                        else
                        {
                            string[] splitCode = nofaktur.Split('-');
                            nofaktur = "SO-" + dt.ToString("yyMMdd") + "-" + (Int32.Parse(splitCode[2]) + 1).ToString().PadLeft(5, '0');
                        }
                        string queryInsert = "INSERT INTO t_salesorder(SOCode, PostingDate, DocumentDate, DeliveryDate, CustomerCode, CustomerName, Notes, Status, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy) " +
                                       "VALUES('" + nofaktur + "', STR_TO_DATE('" + postingDate + "', '%d-%m-%Y')" + ", STR_TO_DATE('" + documentDate + "', '%d-%m-%Y')" + ", STR_TO_DATE('" + deliveryDate + "', '%d-%m-%Y')" +
                                       ", '" + customerCode + "', '" + customerName + "', '" + notes + "', 0, '" +
                                       dt.ToString("yyyy-MM-dd HH:mm:ss") + "', " + userId + ", '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "', " + userId + ")";
                        using (MySqlCommand cmd = new MySqlCommand(queryInsert))
                        {
                            cmd.Connection = con;
                            con.Open();
                            using (MySqlDataReader sdr = cmd.ExecuteReader())
                            {
                                while (sdr.Read())
                                {

                                }
                            }
                            con.Close();
                        }
                        string[] splitItem = items.Split(';');
                        for (int i = 0; i < splitItem.Length; i++)
                        {
                            string[] splitPart = splitItem[i].Split('$');
                            string queryInsertDetail = "INSERT INTO t_salesorderdetail(ItemCode, ItemName, ItemQty, ItemPrice, SOCode) " +
                                       "VALUES('" + splitPart[0] + "', '" + splitPart[1] + "', " + splitPart[2] + ", " + splitPart[3].Replace(",","") +
                                       ", '" + nofaktur + "')";
                            using (MySqlCommand cmd = new MySqlCommand(queryInsertDetail))
                            {
                                cmd.Connection = con;
                                con.Open();
                                using (MySqlDataReader sdr = cmd.ExecuteReader())
                                {
                                    while (sdr.Read())
                                    {

                                    }
                                }
                                con.Close();
                            }
                            total = total + (Convert.ToInt32(splitPart[2]) * Convert.ToDouble(splitPart[3].Replace(",", "")));
                        }
                    }
                    if (total <= Convert.ToDouble(creditLimit))
                    {
                        transaction.Complete();
                        transaction.Dispose();
                        messages = "Document " + nofaktur + " Has Been Created..";
                    }
                    else
                    {
                        transaction.Dispose();
                        messages = "Credit limit too low";
                        isValid = false;
                    }
                }
                catch (Exception ex)
                {
                    messages = "Error Failed To Create User";
                    transaction.Dispose();
                    isValid = false;
                }
            }
            var obj = new
            {
                valid = isValid,
                pesansingkat = messages
            };
            return Json(obj);
        }

        public ActionResult UpdateSalesOrder(string code, string postingDate, string documentDate, string deliveryDate, string notes, string customerCode, string customerName, string creditLimit, string items)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login");
            }
            bool isValid = true;
            string messages = "";
            double total = 0;
            long userId = long.Parse(Session["userId"].ToString());
            DateTime dt = DateTime.Now;
            var txtOptions = new TransactionOptions();
            txtOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, txtOptions))
            {
                try
                {
                    using (MySqlConnection con = new MySqlConnection(constr))
                    {
                        string queryInsert = "UPDATE t_salesorder SET PostingDate = STR_TO_DATE('" + postingDate + "', '%d-%m-%Y')" +
                                             ", DocumentDate = STR_TO_DATE('" + documentDate + "', '%d-%m-%Y')" +
                                             ", DeliveryDate = STR_TO_DATE('" + deliveryDate + "', '%d-%m-%Y')" +
                                             ", CustomerCode = '" + customerCode +
                                             "', CustomerName = '" + customerName +
                                             "', Notes = '" + notes +
                                             "', ModifiedOn = '" + dt.ToString("yyyy-MM-dd HH:mm:ss") +
                                             "', ModifiedBy = " + userId + " WHERE SOCode = '" + code + "'";
                        using (MySqlCommand cmd = new MySqlCommand(queryInsert))
                        {
                            cmd.Connection = con;
                            con.Open();
                            using (MySqlDataReader sdr = cmd.ExecuteReader())
                            {
                                while (sdr.Read())
                                {

                                }
                            }
                            con.Close();
                        }
                        string queryDelete = "DELETE FROM t_salesorderdetail WHERE SOCode = '" + code + "'";
                        using (MySqlCommand cmd = new MySqlCommand(queryDelete))
                        {
                            cmd.Connection = con;
                            con.Open();
                            using (MySqlDataReader sdr = cmd.ExecuteReader())
                            {
                                while (sdr.Read())
                                {

                                }
                            }
                            con.Close();
                        }
                        string[] splitItem = items.Split(';');
                        for (int i = 0; i < splitItem.Length; i++)
                        {
                            string[] splitPart = splitItem[i].Split('$');
                            string queryInsertDetail = "INSERT INTO t_salesorderdetail(ItemCode, ItemName, ItemQty, ItemPrice, SOCode) " +
                                       "VALUES('" + splitPart[0] + "', '" + splitPart[1] + "', " + splitPart[2] + ", " + splitPart[3].Replace(",", "") +
                                       ", '" + code + "')";
                            using (MySqlCommand cmd = new MySqlCommand(queryInsertDetail))
                            {
                                cmd.Connection = con;
                                con.Open();
                                using (MySqlDataReader sdr = cmd.ExecuteReader())
                                {
                                    while (sdr.Read())
                                    {

                                    }
                                }
                                con.Close();
                            }
                            total = total + (Convert.ToInt32(splitPart[2]) * Convert.ToDouble(splitPart[3].Replace(",", "")));
                        }
                    }
                    if (total <= Convert.ToDouble(creditLimit))
                    {
                        transaction.Complete();
                        transaction.Dispose();
                        messages = "Document " + code + " Has Been Updated..";
                    }
                    else
                    {
                        transaction.Dispose();
                        messages = "Credit limit too low";
                        isValid = false;
                    }
                }
                catch (Exception ex)
                {
                    messages = "Error Failed To Create Sales Order";
                    transaction.Dispose();
                    isValid = false;
                }
            }
            var obj = new
            {
                valid = isValid,
                pesansingkat = messages
            };
            return Json(obj);
        }

        public ActionResult UpdateStatusSalesOrder(string id, string status)
        {
            if (Session["userId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            bool isValid = true;
            string messages = "";
            int checkMinus = 0;
            double userId = long.Parse(Session["userId"].ToString());
            DateTime dt = DateTime.Now;
            var txOptions = new TransactionOptions();
            txOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            using (var transaction = new TransactionScope(TransactionScopeOption.Required, txOptions))
            {
                try
                {
                    using (MySqlConnection con = new MySqlConnection(constr))
                    {
                        string query = "UPDATE t_salesorder SET Status = " + Convert.ToInt32(status) + ", ModifiedBy = " + userId +
                            ", ModifiedOn = '" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "' WHERE SOCode = '" + id + "'";
                        MySqlCommand cmdUpdate = new MySqlCommand(query);
                        cmdUpdate.Connection = con;
                        con.Open();
                        cmdUpdate.ExecuteNonQuery();
                        cmdUpdate.Dispose();
                        con.Close();
                        if (status == "1")
                        {
                            List<SalesOrderDetailsClass> salesOrderDetailsClasses = new List<SalesOrderDetailsClass>();
                            string queryGetDetail = "SELECT a.ItemCode, a.ItemQty, b.ItemStock " + 
                                                    "FROM t_salesorderdetail a " + 
                                                    "JOIN m_item b ON b.ItemCode = a.ItemCode " + 
                                                    "WHERE SOCode = '" + id + "'";
                            using (MySqlCommand cmd = new MySqlCommand(queryGetDetail))
                            {
                                cmd.Connection = con;
                                con.Open();
                                using (MySqlDataReader sdr = cmd.ExecuteReader())
                                {
                                    while (sdr.Read())
                                    {
                                        SalesOrderDetailsClass salesOrderDetailsClass = new SalesOrderDetailsClass();
                                        salesOrderDetailsClass.itemCode = sdr["ItemCode"].ToString();
                                        salesOrderDetailsClass.itemQty = Convert.ToInt32(sdr["ItemQty"].ToString());
                                        salesOrderDetailsClass.itemStock = Convert.ToInt32(sdr["ItemStock"].ToString());
                                        salesOrderDetailsClasses.Add(salesOrderDetailsClass);
                                        if (salesOrderDetailsClass.itemQty > salesOrderDetailsClass.itemStock)
                                        {
                                            checkMinus = 1;
                                        }
                                    }
                                }
                                con.Close();
                            }
                            for (int i = 0; i < salesOrderDetailsClasses.Count; i++)
                            {
                                string queryUpdateStock = "UPDATE m_item SET ItemStock = (SELECT ItemStock FROM m_item WHERE ItemCode = '" + salesOrderDetailsClasses[i].itemCode + "') - " + 
                                                          salesOrderDetailsClasses[i].itemQty + " WHERE ItemCode = '" + salesOrderDetailsClasses[i].itemCode + "'";
                                using (MySqlCommand cmd = new MySqlCommand(queryUpdateStock))
                                {
                                    cmd.Connection = con;
                                    con.Open();
                                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                                    {
                                        while (sdr.Read())
                                        {

                                        }
                                    }
                                    con.Close();
                                }
                            }
                        }
                    }
                    if (checkMinus == 0)
                    {
                        transaction.Complete();
                        transaction.Dispose();
                        messages = "Success";
                    }
                    else
                    {
                        transaction.Dispose();
                        messages = "Stock Minus";
                        isValid = false;
                    }
                }
                catch (Exception ex)
                {
                    messages = ex.Message;
                    messages = "Error Failed To update Sales Order";
                    transaction.Dispose();
                    isValid = false;
                }
            }
            var obj = new
            {
                valid = isValid,
                pesansingkat = messages
            };
            return Json(obj);
        }

        public JsonResult GetCustomerFromCode(string customerCode)
        {
            CustomerClass customerClass = new CustomerClass();
            using (MySqlConnection con = new MySqlConnection(constr))
            {
                string query = "SELECT a.CustomerCode, a.CustomerName, a.CustomerPhone, a.CustomerEmail, a.ContactPerson, a.CustomerCity, a.CreditLimit, " +
                               "a.CreditLimit - (SELECT SUM(c.ItemQty * c.ItemPrice) " +
                                                "FROM t_salesorder b " +
                                                "JOIN t_salesorderdetail c ON c.SOCode = b.SOCode " +
                                                "WHERE b.CustomerCode = a.CustomerCode AND b.Status = 0) AS LastCredit " +
                               "FROM m_customer a " +
                               "WHERE a.CustomerCode = '" + customerCode + "'";
                using (MySqlCommand cmd = new MySqlCommand(query))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            customerClass.customerCode = sdr["CustomerCode"].ToString();
                            customerClass.customerName = sdr["CustomerName"].ToString();
                            customerClass.customerPhone = sdr["CustomerPhone"].ToString();
                            customerClass.customerEmail = sdr["CustomerEmail"].ToString();
                            customerClass.contactPerson = sdr["ContactPerson"].ToString();
                            customerClass.customerCity = sdr["CustomerCity"].ToString();
                            customerClass.lastCreditLimit = Convert.ToDouble(sdr["LastCredit"].ToString().Length == 0 ? sdr["CreditLimit"].ToString() : sdr["LastCredit"].ToString());
                            customerClass.strLastCreditLimit = Convert.ToDecimal(customerClass.lastCreditLimit).ToString("#,###");
                            customerClass.creditLimit = Convert.ToDouble(sdr["CreditLimit"].ToString());
                            customerClass.strCreditLimit = Convert.ToDecimal(customerClass.creditLimit).ToString("#,###");
                        }
                    }
                    con.Close();
                }
                return Json(customerClass, JsonRequestBehavior.AllowGet);
            }
        }
    }
}