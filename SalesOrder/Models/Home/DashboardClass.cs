﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesOrder.Models.Home
{
    public class DashboardClass
    {
        public int newClient { get; set; }
        public int openOrders { get; set; }
        public int cancelOrders { get; set; }
        public int closeOrders { get; set; }
    }

    public class NewCustomer
    {
        public string customerCode { get; set; }
        public int totalSO { get; set; }
    }
}