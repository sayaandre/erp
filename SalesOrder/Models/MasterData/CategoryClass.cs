﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesOrder.Models.MasterData
{

    public class CategoryListClass
    {
        public List<CategoryClass> categories { get; set; }
    }

    public class CategoryClass
    {
        public string categoryCode { get; set; }
        public string categoryName { get; set; }
        public int totalItem { get; set; }
        public string isActive { get; set; }
    }

    public class AddCategoryClass
    {
        public string categoryCode { get; set; }
        public string categoryName { get; set; }
    }

    public class DetailsCategoryClass
    {
        public string categoryCode { get; set; }
        public string categoryName { get; set; }
        public string isActive { get; set; }
        public int totalItem { get; set; }
        public List<ItemClass> items { get; set; }
    }

}