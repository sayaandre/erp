﻿using SalesOrder.Models.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesOrder.Models.MasterData
{

    public class CustomerListClass
    {
        public List<CustomerClass> customers { get; set; }
    }

    public class AddCustomerClass
    {
        public string customerName { get; set; }
        public string customerPhone { get; set; }
        public string customerEmail { get; set; }
        public string contactPerson { get; set; }
        public string customerCity { get; set; }
        public double creditLimit { get; set; }
    }

    public class DetailsCustomerClass
    {
        public string customerCode { get; set; }
        public string customerName { get; set; }
        public string customerPhone { get; set; }
        public string customerEmail { get; set; }
        public string contactPerson { get; set; }
        public double creditLimit { get; set; }
        public string strCreditLimit { get; set; }
        public string customerCity { get; set; }
        public string isActive { get; set; }
        public List<SalesOrderClass> soes { get; set; }
    }

    public class CustomerClass
    {
        public string customerCode { get; set; }
        public string customerName { get; set; }
        public string customerPhone { get; set; }
        public string customerEmail { get; set; }
        public string contactPerson { get; set; }
        public string customerCity { get; set; }
        public double creditLimit { get; set; }
        public string strCreditLimit { get; set; }
        public double lastCreditLimit { get; set; }
        public string strLastCreditLimit { get; set; }
        public string isActive { get; set; }
    }
}