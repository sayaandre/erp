﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesOrder.Models.MasterData
{
    public class ItemListClass
    {
        public List<ItemClass> items { get; set; }
    }

    public class AddItemClass
    {
        public List<CategoryClass> categories { get; set; }
        public string itemName { get; set; }
        public double itemPrice { get; set; }
        public int itemStock { get; set; }
        public string categoryCode { get; set; }
    }

    public class DetailsItemClass
    {
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public double itemPrice { get; set; }
        public string itemPriceStr { get; set; }
        public int itemStock { get; set; }
        public string categoryCode { get; set; }
        public string categoryName { get; set; }
        public string isActive { get; set; }
        public List<CategoryClass> categories { get; set; }
    }

    public class ItemClass
    {
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public double itemPrice { get; set; }
        public string itemPriceStr { get; set; }
        public int itemStock { get; set; }
        public string categoryCode { get; set; }
        public string categoryName { get; set; }
        public string isActive { get; set; }
    }
}