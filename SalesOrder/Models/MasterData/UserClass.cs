﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesOrder.Models.MasterData
{
    public class UserListClass
    {
        public List<UserClass> users { get; set; }
    }

    public class AddUserClass
    {
        public string userName { get; set; }
        public string userRole { get; set; }
        public string userPhone { get; set; }
        public string userEmail { get; set; }
        public string userAddress { get; set; }
        public string userCity { get; set; }
    }

    public class DetailsUserClass
    {
        public string userCode { get; set; }
        public string userName { get; set; }
        public string userRole { get; set; }
        public string userPhone { get; set; }
        public string userEmail { get; set; }
        public string userAddress { get; set; }
        public string userCity { get; set; }
        public string isActive { get; set; }
    }

    public class UserClass
    {
        public string userCode { get; set; }
        public string userName { get; set; }
        public string userPassword { get; set; }
        public string userRole { get; set; }
        public string userPhone { get; set; }
        public string userEmail { get; set; }
        public string userAddress { get; set; }
        public string userCity { get; set; }
        public string isActive { get; set; }
    }
}