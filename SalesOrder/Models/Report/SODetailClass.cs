﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesOrder.Models.Report
{
    public class SODetailClass
    {
        public string statusCode { get; set; }
        public List<GetStatusClass> statues { get; set; }
    }

    public class GetReportSalesOrderDetail
    {
        public string SOCode { get; set; }
        public string postingDate { get; set; }
        public string documentDate { get; set; }
        public string deliveryDate { get; set; }
        public string customerCode { get; set; }
        public string customerName { get; set; }
        public string notes { get; set; }
        public string status { get; set; }
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public int itemQty { get; set; }
        public double itemPrice { get; set; }
        public string strItemPrice { get; set; }
    }

    public class GetStatusClass
    {
        public string statusCode { get; set; }
        public string statusName { get; set; }
    }
}