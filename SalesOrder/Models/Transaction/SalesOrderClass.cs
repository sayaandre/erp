﻿using SalesOrder.Models.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesOrder.Models.Transaction
{
    public class ListSalesOrderClass
    {
        public List<SalesOrderClass> soes { get; set; }
    }

    public class AddSalesOrderClass
    {
        public string postingDate { get; set; }
        public string documentDate { get; set; }
        public string deliveryDate { get; set; }
        public string customerCode { get; set; }
        public string customerName { get; set; }
        public string notes { get; set; }
        public List<CustomerClass> customers { get; set; }
        public List<ItemClass> items { get; set; }
    }

    public class DetailsSalesOrderClass
    {
        public double soId { get; set; }
        public string soCode { get; set; }
        public string postingDate { get; set; }
        public string documentDate { get; set; }
        public string deliveryDate { get; set; }
        public string customerCode { get; set; }
        public string customerName { get; set; }
        public string customerPhone { get; set; }
        public string customerEmail { get; set; }
        public string customerCity { get; set; }
        public string strCreditLimit { get; set; }
        public double creditLimit { get; set; }
        public string strLastCreditLimit { get; set; }
        public double lastCreditLimit { get; set; }
        public string notes { get; set; }
        public string status { get; set; }
        public List<SalesOrderDetailsClass> details { get; set; }
        public List<CustomerClass> customers { get; set; }
        public List<ItemClass> items { get; set; }
    }

    public class SalesOrderClass
    {
        public string soCode { get; set; }
        public string postingDate { get; set; }
        public string documentDate { get; set; }
        public string deliveryDate { get; set; }
        public string customerCode { get; set; }
        public string customerName { get; set; }
        public string notes { get; set; }
        public string status { get; set; }
        public int totalItem { get; set; }
    }

    public class SalesOrderDetailsClass
    {
        public double soDetailId { get; set; }
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public int itemQty { get; set; }
        public double itemPrice { get; set; }
        public string itemPriceStr { get; set; }
        public string soCode { get; set; }
        public string categoryName { get; set; }
        public int itemStock { get; set; }
    }
}