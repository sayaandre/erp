-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 10, 2021 at 07:00 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `salesorder`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_category`
--

CREATE TABLE `m_category` (
  `CategoryCode` varchar(20) NOT NULL,
  `CategoryName` varchar(100) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `CreatedBy` bigint(20) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` bigint(20) NOT NULL,
  `ModifiedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_category`
--

INSERT INTO `m_category` (`CategoryCode`, `CategoryName`, `IsActive`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
('MSG', 'Micin', 1, 1, '2021-09-07 15:45:49', 1, '2021-09-07 15:49:43'),
('Cair', 'Benda Cair', 1, 1, '2021-09-09 20:41:27', 1, '2021-09-09 20:41:27');

-- --------------------------------------------------------

--
-- Table structure for table `m_customer`
--

CREATE TABLE `m_customer` (
  `CustomerId` bigint(20) NOT NULL,
  `CustomerCode` varchar(20) NOT NULL,
  `CustomerName` varchar(100) NOT NULL,
  `CustomerPhone` varchar(20) NOT NULL,
  `CustomerEmail` varchar(100) NOT NULL,
  `ContactPerson` varchar(100) NOT NULL,
  `CustomerCity` varchar(100) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `CreditLimit` int(11) NOT NULL,
  `CreatedBy` bigint(20) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` bigint(20) NOT NULL,
  `ModifiedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_customer`
--

INSERT INTO `m_customer` (`CustomerId`, `CustomerCode`, `CustomerName`, `CustomerPhone`, `CustomerEmail`, `ContactPerson`, `CustomerCity`, `IsActive`, `CreditLimit`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 'JAK-00001', 'PT. Kanan Kiri', '08211412431', 'andre@gmail.com', 'Andre Ferando', 'Jakarta', 1, 50000000, 1, '2021-09-07 16:28:49', 1, '2021-09-07 17:01:48'),
(2, 'JAK-00002', 'Testing', '082132141', 'test@gmail.com', 'Test', 'Jakarta', 1, 10000000, 1, '2021-09-09 20:40:56', 1, '2021-09-09 20:40:56'),
(3, 'BAN-00001', 'Qwerty', '0812314124', 'qwerty@gmail.com', 'Qwerty', 'Bandung', 1, 100000, 1, '2021-09-10 06:49:24', 1, '2021-09-10 06:49:24');

-- --------------------------------------------------------

--
-- Table structure for table `m_item`
--

CREATE TABLE `m_item` (
  `ItemId` bigint(20) NOT NULL,
  `ItemCode` varchar(20) NOT NULL,
  `ItemName` varchar(100) NOT NULL,
  `CategoryCode` varchar(20) NOT NULL,
  `ItemStock` int(11) NOT NULL,
  `ItemPrice` double NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `CreatedBy` bigint(20) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` bigint(20) NOT NULL,
  `ModifiedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_item`
--

INSERT INTO `m_item` (`ItemId`, `ItemCode`, `ItemName`, `CategoryCode`, `ItemStock`, `ItemPrice`, `IsActive`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 'MSG-00001', 'Chitato', 'MSG', 750, 15000, 1, 1, '2021-09-07 15:50:05', 1, '2021-09-07 15:50:05'),
(2, 'MSG-00002', 'Lays', 'MSG', 497, 8000, 1, 1, '2021-09-08 11:43:02', 1, '2021-09-08 11:43:02'),
(3, 'Cair-00001', 'Sirup', 'Cair', 40, 10000, 1, 1, '2021-09-09 20:42:46', 1, '2021-09-10 06:44:37');

-- --------------------------------------------------------

--
-- Table structure for table `m_stringmap`
--

CREATE TABLE `m_stringmap` (
  `StringId` bigint(20) NOT NULL,
  `StringName` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_stringmap`
--

INSERT INTO `m_stringmap` (`StringId`, `StringName`) VALUES
(0, 'Open'),
(1, 'Close'),
(2, 'Cancel');

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE `m_user` (
  `UserId` bigint(20) NOT NULL,
  `UserCode` varchar(20) NOT NULL,
  `UserName` varchar(100) NOT NULL,
  `UserPassword` varchar(500) NOT NULL,
  `UserRole` varchar(100) NOT NULL,
  `UserPhone` varchar(20) NOT NULL,
  `UserEmail` varchar(100) NOT NULL,
  `UserAddress` varchar(500) NOT NULL,
  `UserCity` varchar(100) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `CreatedBy` bigint(20) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` bigint(20) NOT NULL,
  `ModifiedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`UserId`, `UserCode`, `UserName`, `UserPassword`, `UserRole`, `UserPhone`, `UserEmail`, `UserAddress`, `UserCity`, `IsActive`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 'admin', 'admin', 'd77Dmve/9m58spjc7aUSPg==', 'SA', '', '', '', '', 1, 1, '2021-09-07 08:38:37', 1, '2021-09-07 08:38:37'),
(2, 'JAK-00001', 'Andre', 'd77Dmve/9m58spjc7aUSPg==', 'Sales', '081253523423', 'andreferando@gmail.com', 'Puri kembangan', 'Jakarta', 1, 1, '2021-09-07 19:24:05', 1, '2021-09-08 09:28:49');

-- --------------------------------------------------------

--
-- Table structure for table `t_salesorder`
--

CREATE TABLE `t_salesorder` (
  `SOCode` varchar(20) NOT NULL,
  `PostingDate` date NOT NULL,
  `DocumentDate` date DEFAULT NULL,
  `DeliveryDate` date DEFAULT NULL,
  `CustomerCode` varchar(20) NOT NULL,
  `CustomerName` varchar(100) NOT NULL,
  `Total` double NOT NULL,
  `Notes` varchar(500) DEFAULT NULL,
  `Status` int(11) NOT NULL,
  `CreatedBy` bigint(20) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` bigint(20) NOT NULL,
  `ModifiedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_salesorder`
--

INSERT INTO `t_salesorder` (`SOCode`, `PostingDate`, `DocumentDate`, `DeliveryDate`, `CustomerCode`, `CustomerName`, `Total`, `Notes`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
('SO-210910-00001', '2021-09-10', '2021-09-10', '2021-09-10', 'JAK-00002', 'Testing', 0, '', 0, 1, '2021-09-10 07:51:03', 1, '2021-09-10 07:51:03'),
('SO-210910-00002', '2021-09-10', '2021-09-10', '2021-09-10', 'JAK-00001', 'PT. Kanan Kiri', 0, '', 0, 1, '2021-09-10 08:01:49', 1, '2021-09-10 08:01:49'),
('SO-210910-00003', '2021-09-10', '2021-09-10', '2021-09-10', 'BAN-00001', 'Qwerty', 0, '', 0, 1, '2021-09-10 11:58:03', 1, '2021-09-10 11:58:03'),
('SO-210911-00001', '2021-09-10', '2021-09-10', '2021-09-10', 'BAN-00001', 'Qwerty', 0, '', 0, 1, '2021-09-11 11:34:03', 1, '2021-09-10 11:57:40');

-- --------------------------------------------------------

--
-- Table structure for table `t_salesorderdetail`
--

CREATE TABLE `t_salesorderdetail` (
  `ItemCode` varchar(20) NOT NULL,
  `ItemName` varchar(100) NOT NULL,
  `ItemQty` int(11) NOT NULL,
  `ItemPrice` double NOT NULL,
  `SOCode` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_salesorderdetail`
--

INSERT INTO `t_salesorderdetail` (`ItemCode`, `ItemName`, `ItemQty`, `ItemPrice`, `SOCode`) VALUES
('MSG-00002', 'Lays', 1, 8000, 'SO-210910-00001'),
('MSG-00002', 'Lays', 1, 8000, 'SO-210910-00002'),
('MSG-00001', 'Chitato', 5, 15000, 'SO-210911-00001'),
('MSG-00002', 'Lays', 1, 8000, 'SO-210910-00003');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_customer`
--
ALTER TABLE `m_customer`
  ADD PRIMARY KEY (`CustomerId`);

--
-- Indexes for table `m_item`
--
ALTER TABLE `m_item`
  ADD PRIMARY KEY (`ItemId`);

--
-- Indexes for table `m_user`
--
ALTER TABLE `m_user`
  ADD PRIMARY KEY (`UserId`);

--
-- Indexes for table `t_salesorder`
--
ALTER TABLE `t_salesorder`
  ADD PRIMARY KEY (`SOCode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_customer`
--
ALTER TABLE `m_customer`
  MODIFY `CustomerId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_item`
--
ALTER TABLE `m_item`
  MODIFY `ItemId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_user`
--
ALTER TABLE `m_user`
  MODIFY `UserId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
